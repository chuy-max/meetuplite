High Priority:
-Add day picker.
-Sort columns.
-Always serve https

Medium priority:
-Stop fetching data when last event is from a different day.
-Add status bar with number of events.
-Move controls to right side.
-Add meetup user authentication? only if successful.

Low priority:
-Only send a server request on real date and distance change.
 Currently sending when changing windows, hovering, clicking and unclicking.

Complete:
-Get google maps from https.
-Add user location
-Filter by min rsvp.
-Filter by category.
-Remove events not matching date.
-Filter by date.
-Filter by max distance.
-Use update button to update fields automatically.
-Add link to event.
-Add id column.
-Fetch two pages.
-Fix # column when fetching two pages.
-Update fields automatically upon field change.
  -Update max distance automatically.
  -Update date automatically.

Auth:
https://secure.meetup.com/oauth2/authorize?client_id=YOUR_CONSUMER_KEY&response_type=code&redirect_uri=YOUR_CONSUMER_REDIRECT_URI
