function FindEventRequest(){
}

FindEventRequest.prototype.toString = function(){
    var host = "https://api.meetup.com";
    var key = "&key=204e194f3f5771611532305058387f4d";
    var date = $("#date").val();
    var distance = $("#maxDistance").val();
    var lat = MeetupLite.location.latitude;
    var lon = MeetupLite.location.longitude;

    var request = "/find/events?fields=group_category&radius=" +
      distance +
      "&lat=" +
      lat +
      "&lon=" +
      lon +
      "&scroll=since:" +
      date +
      "T00:00:00.000-04:00";
    var url = host+request+key;
    return url;;
};
