function EventTableController(filter){
    this.eventList = [];
    this.filter = filter;
}

EventTableController.prototype.giveEventList = function(eventList){
    this.eventList = eventList;
    $("#eventTable tbody tr").remove();
    this.regenerateTable();
};

EventTableController.prototype.addEventsToTable = function(){
    var filteredList = this.filter.getFilteredList(this.eventList);

    var count = $('#eventTable tbody tr').length;
    for (var index in filteredList) {
        event = filteredList[index];
        date = unixTimeToDateString(event['time']);
        $('#eventTable').find('tbody').append( "<tr>" + 
                "<td>" + (parseInt(index, 10)+1+count) + "</td>" +
                "<td>" + date + "</td>" +
                "<td>" + event['group']['category']['name'] + "</td>" +
                "<td>" + event['group']['name'] + "</td>" +
                "<td><a href=\""+ event.link +"\">" + event.name + "</a></td>" +
                "<td>" + event.yes_rsvp_count + "</td>" +
                "</tr>" );
    }
};

EventTableController.prototype.regenerateTable = function(){
    $("#eventTable tbody tr").remove();
    this.addEventsToTable();
};

