var MeetupLite = {
    request: new FindEventRequest(),
    filter: new EventFilter(),
}
MeetupLite.eventTableController = new EventTableController(MeetupLite.filter);
MeetupLite.fetchData = function() {
    var eventList = [];
    sendRequest(MeetupLite.request.toString(), function(response) {
        eventList = $.merge(eventList, response.data);

        // Second request.
        sendRequest(response.meta.next_link, function(response) {
            eventList = $.merge(eventList, response.data);
            MeetupLite.eventTableController.giveEventList(eventList);
        });
    });
}
MeetupLite.location = {};
MeetupLite.location.latitude = 0.0;
MeetupLite.location.longitude = 0.0;

function unixTimeToDateString(timestamp) {
    var dateTime = new Date(timestamp);
    return dateTime.toLocaleString();
}

function sendRequest(url, callback) {
    console.log("Sending request to: " + url);
    $.ajax({
        url: url,
        dataType: 'jsonp',
        success: callback
      });
}

$(document).ready(function() {
    console.log("Site ready");
    document.getElementById('date').valueAsDate = new Date();

    $('#category').keyup( $.debounce( 250, function() {
        console.log("Category change triggering table filtering.");
        MeetupLite.eventTableController.regenerateTable();
    }));
    $("#minRsvpCount").on("keyup change", $.debounce( 250, function() {
        console.log("RSVP change triggering table filtering.");
        MeetupLite.eventTableController.regenerateTable();
    }));
    $("#date").on("change", $.debounce( 250, function() {
        console.log("Date change triggering data retrieval.");
        MeetupLite.fetchData();
    }));
    $("#maxDistance").on("change", $.debounce( 250, function() {
        console.log("Distance change triggering data retrieval.");
        MeetupLite.fetchData();
    }));

    setInitialLocation();    
});


function setInitialLocation() {
    console.log("Sending location request to geoip-db");
    $.getJSON('https://geoip-db.com/json/geoip.php?jsonp=?') 
    .done (function(location) {
       console.log("geoip-db request received");
       initializeMap(location.latitude, location.longitude);
    });
}

function initializeMap(initialLat, initialLon) {
    console.log("Initializing map: " + initialLat + " " + initialLon);
    MeetupLite.location.latitude = initialLat;
    MeetupLite.location.longitude = initialLon;
    $('#map').locationpicker({
        location: {
            latitude: initialLat,
            longitude: initialLon
        },
        radius: 10,
        zoom: 8,
        onchanged: onLocationChanged
    });
    MeetupLite.fetchData();    
}

function showPosition(position) {
    initializeMap(position.coords.latitude, position.coords.longitude);
}

function onLocationChanged(currentLocation, radius, isMarkerDropped) {
    console.log("Map location change triggering data retrieval.");
    MeetupLite.location = currentLocation;
    MeetupLite.fetchData();
}
