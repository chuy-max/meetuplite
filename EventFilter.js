function EventFilter(){
    this.filterMethods = [
        this.hasValidDate,
        this.hasValidCategory,
        this.hasValidRsvpCount];
}

EventFilter.prototype.getFilteredList = function(eventList){
    var filteredList = [];

    var i; var j;
    for(i=0; i<eventList.length; i++) {
        if(this.isValidEvent(eventList[i])) {
            filteredList.push(eventList[i]);
        }
    }

    return filteredList;
};

EventFilter.prototype.isValidEvent = function(event) {
    var i;
    for(i=0; i<this.filterMethods.length; i++) {
        if(!this.filterMethods[i](event)) {
            return false;
        }
    }
    return true;
}

EventFilter.prototype.hasValidDate = function(event) {
    var eventDate = new Date(event['time']);
    eventDate.setHours(0,0,0,0);
    var filterDate = new Date($("#date").val().replace(/-/g, "/"));
    return eventDate.getTime() === filterDate.getTime();
}

EventFilter.prototype.hasValidCategory = function(event) {
    var filterCategory = $("#category").val();
    var eventCategory = event['group']['category']['name'];

    if(filterCategory === "")
        return true;

    if(eventCategory.indexOf(filterCategory) !== -1)
        return true;

    return false;
}

EventFilter.prototype.hasValidRsvpCount = function(event) {
    var filterMinRsvpCount = $("#minRsvpCount").val();
    var eventRsvpCount = event.yes_rsvp_count;
    return eventRsvpCount >= filterMinRsvpCount;
}

